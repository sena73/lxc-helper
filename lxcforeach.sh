#!/bin/sh

SUDOROOT=
SUDOLXCUSER=
UNAME="$(id -un)"
if [ "$UNAME" != "root" ]; then
  SUDOROOT=sudo
fi
if [ "$UNAME" != "lxcuser" ]; then
  SUDOLXCUSER="sudo -u lxcuser"
fi


for ii in /var/lib/lxc/*; do
  if [ -d $ii ]; then
    SUDO=
    LXC=$(basename $ii); 
    echo "executing for lxc $LXC:"
    $SUDOROOT lxc-attach -n $LXC -- $@
  fi
done

for ii in /home/lxcuser/.local/share/lxc/*; do
  if [ -d $ii ]; then
    LXC=$(basename $ii);
    echo "executing for lxc $LXC:"
    $SUDOLXCUSER lxc-unpriv-attach -n $LXC -- "$@"
  fi
done

