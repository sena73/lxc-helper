#!/bin/sh
# too small for copyright notice

GetMAC()
{
  if [ -n "$1" ]; then
    OID="00:16:3e"
    RAND=$(echo $1 | md5sum | sed 's/\(..\)\(..\)\(..\).*/\1:\2:\3/')
    echo "$OID:$RAND"
  else
    echo "ERROR: please supply hostname to create MAC address from, e.g.:"
    echo "       $FUNCNAME myhost"
  fi
}

BDIR=/home/lxcuser/.local/share/lxc

for ii in ${BDIR}/*/config; do
  name="$(echo "${ii}" | sed -e "s|${BDIR}/\([^/]\+\)/config|\1|")"
  mac=$(GetMAC $name)
  unset COMMENT
  if (grep -q 'please do not change hwmac' $ii); then
    COMMENT=" (hwmac not updated, because found comment 'please do not change hwmac')"
  else
    if (grep -q 'lxc.net.0.hwaddr' $ii); then
      sed --in-place "s/lxc.net.0.hwaddr *=.*/lxc.net.0.hwaddr = ${mac}/" $ii
    else
      echo "lxc.net.0.hwaddr = ${mac}" >> $ii
    fi
  fi
  echo "MAC for ${name}: $(grep 'lxc.net.0.hwaddr' $ii)${COMMENT}"
done
